import * as importMap from "https://esm.sh/esbuild-plugin-import-map";
import {
  build as esbuild,
  Plugin,
} from "https://deno.land/x/esbuild/mod.js";
import * as path from "https://deno.land/std/path/mod.ts";
import httpFetch from "https://deno.land/x/esbuild_plugin_http_fetch/index.js";
import { Logger } from "./logger.ts";
import { httpImports } from "./httpImports.ts";
// import sveltePlugin from "./svelte.ts";

export interface WeebConfig {
  useImportMap?: boolean;
  useSvelte?: boolean;
}

export class WeebBuilder {
  public readonly logger = new Logger({
    level: "debug",
  });

  constructor(public readonly config: WeebConfig) {}

  protected async compile(sourcePath: string, outPath: string) {
    const process = Deno.run({
      cmd: ["deno", "bundle", sourcePath, outPath],
    });

    await process.status();
  }

  async build() {
    // TODO: esbuild?
    /*     const plugins: Plugin[] = [];

    if (this.config.useImportMap) {
      const importMapPath = path.join(Deno.cwd(), "import_map.json");
      importMap.load(JSON.parse(await Deno.readTextFile(importMapPath)));
      plugins.push(importMap.plugin());
    }

    if (this.config.useSvelte) {
      const sveltePlugin = await import("./svelte.ts");
      plugins.push(sveltePlugin.default());
    } */

    const sourceFile = path.join(Deno.cwd(), "src/main.ts");
    const distFile = path.join(Deno.cwd(), "dist/bundle.js");
    // await this.compile(sourceFile, distFile);

    const importMapPath = path.join(Deno.cwd(), "import_map.json");

    const result = await Deno.emit(sourceFile, {
      bundle: "module",
      check: true,
      compilerOptions: {
        lib: ["deno.unstable", "deno.ns", "dom"],
      },
      importMapPath,
    });

    if (result.diagnostics.length > 0) {
      for (const diagnostic of result.diagnostics) {
        this.logger.error(diagnostic.messageText);
      }
    }

    const distDir = path.dirname(distFile);

    try {
      await Deno.mkdir(distDir, { recursive: true });
    } catch (e) {
      // if exists already, continue
    }
    
    for (const file of Object.keys(result.files)) {
      await Deno.writeTextFile(
        path.join(distDir, file.replace("deno://", "")),
        result.files[file]
      );
    }

    this.logger.info("Built successfully.");
  }
}
