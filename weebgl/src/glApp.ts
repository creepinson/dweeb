import { Application } from "./app.ts";
import { Renderable } from "./component/renderable.ts";
import { createBasicShader } from "./renderEngine/shader.ts";
import { Transform } from "./math/transform.ts";
import { Tensor } from "./math/tensor.ts";

export class GlApplication extends Application {
  readonly gl: WebGL2RenderingContext;

  constructor(public readonly canvas: HTMLCanvasElement) {
    super();
    const gl = canvas.getContext("webgl2");
    if (!gl) throw new Error("WebGL2 not supported");
    this.gl = gl;
  }

  render() {
    this.gl.clearColor(0, 0, 0, 1);
    this.gl.clear(this.gl.COLOR_BUFFER_BIT);

    const shader = createBasicShader(this.gl);

    // TODO: camera component
    // create camera matrix using perspective function
    const cameraMatrix = Tensor.perspective(
      60,
      this.gl.drawingBufferWidth / this.gl.drawingBufferHeight,
      0.1,
      1000
    );

    for (const entity of this.ecs.getEntities()) {
      const container = this.ecs.getComponents(entity);
      if (!container) throw new Error("no components");

      // Make sure the entity has the required components before attempting to render it
      if (!container.has(Transform) || !container.has(Renderable)) continue;

      const eTransform = container.get(Transform);
      const renderable = container.get(Renderable);
      // TODO: shader should be a component

      shader.use();
      shader.uModelMatrix.set(false, eTransform.matrix.data);
      shader.uCameraMatrix.set(false, cameraMatrix.data);
      shader.inPosition.enable();
      shader.inPosition.set(3, this.gl.FLOAT, false, 0, 0);
      shader.uColor.setV(renderable.material.color.data);

      renderable.model.draw();
    }
  }

  async run() {
    this.render();
    // console.log("running");
    super.run();
  }
}
