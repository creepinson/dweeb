import { ECS } from "./ecs/ecs.ts";

export class Application {
  readonly ecs: ECS;
  running = false;

  constructor() {
    this.ecs = new ECS();
  }

  start() {
    this.running = true;
    this.run();
  }

  protected async run() {
    this.ecs.update();
    if (this.running) requestAnimationFrame(this.run.bind(this));
  }
}
