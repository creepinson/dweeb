import { Tensor } from "../math/tensor.ts";

export interface Material {
  color: Tensor<[4]>;
}

export const createDefaultMaterial = (): Material => ({
  color: new Tensor([4], [1, 1, 1, 1]),
});
