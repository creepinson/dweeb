import { Renderable } from "../../weebgl/src/component/renderable.ts";
import { GlApplication } from "../../weebgl/src/glApp.ts";
import { Tensor } from "../../weebgl/src/math/tensor.ts";
import { Transform } from "../../weebgl/src/math/transform.ts";
import * as Model from "../../weebgl/src/renderEngine/model.ts";

const canvas = document.getElementById("canvas") as HTMLCanvasElement;
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const app = new GlApplication(canvas);

const testEntity = app.ecs.addEntity();
app.ecs.addComponent(
  testEntity,
  new Transform(
    Tensor.create([3], [0, 0, -5]),
    Tensor.zeros([3]),
    Tensor.create([3], [1, 1, 1])
  )
);
app.ecs.addComponent(
  testEntity,
  new Renderable(Model.createCube(app.gl), {
    color: Tensor.create([4], [1, 0, 0, 1]),
  })
);

console.info(app.ecs.getEntities());
app.start();
setInterval(() => {
  const container = app.ecs.getComponents(testEntity);
  if (!container) return;
  const transform = container.get(Transform);
  transform.position.data[2] -= 0.1;
}, 100);
